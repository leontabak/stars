package stars;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

public class ColorListener implements ActionListener {

    private final ArtPanel artPanel;
    private final TreeMap<String, Color> palette;

    public ColorListener(ArtPanel artPanel,
            TreeMap<String, Color> palette) {
        this.artPanel = artPanel;
        this.palette = palette;
    } // ColorListener()

    @Override
    public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();

        Color color = this.palette.get(command);

        this.artPanel.setFigureColor(color);
    } // actionPerformed( ActionEvent )

} // ColorListener

