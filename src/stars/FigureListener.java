package stars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

public class FigureListener implements ActionListener {

    private final TreeMap<String, Integer> figures;
    private final ArtPanel artPanel;

    public FigureListener(ArtPanel artPanel,
            TreeMap<String, Integer> figures) {
        this.artPanel = artPanel;
        this.figures = figures;
    } // FigureListener()

    @Override
    public void actionPerformed(ActionEvent event) {

        String command = event.getActionCommand();

        int numberOfPoints = this.figures.get(command);

        this.artPanel.setNumberOfPoints(numberOfPoints);
    } // actionPerformed( ActionEvent )

} // FigureListener
