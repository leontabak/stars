package stars;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.TreeMap;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Art extends JFrame {

    private static final int GUI_WIDTH = 768;
    private static final int GUI_HEIGHT = 768;
    private static final String GUI_TITLE = "GUI";

    private static final int TRANSPARENCY = 0xC0;

    private final ArtPanel artPanel;
    private final TreeMap<String, Color> palette;
    private final TreeMap<String, Double> sizes;
    private final TreeMap<String, Integer> figures;

    public Art() {
        this.sizes = createSizes();
        this.figures = createFigures();
        this.palette = createPalette();

        Color color = (Color) palette.values().toArray()[0];
        int numberOfPoints = (Integer) this.figures.values().toArray()[0];
        double radius = (Double) this.sizes.values().toArray()[0];

        this.artPanel = new ArtPanel(color, numberOfPoints, radius);

        this.configure();
    } // Art()

    private void configure() {
        this.setSize(GUI_WIDTH, GUI_HEIGHT);
        this.setTitle(GUI_TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = this.getContentPane();
        contentPane.add(this.artPanel);

        JMenuBar menuBar = createMenuBar();
        this.setJMenuBar(menuBar);

        this.setVisible(true);
    } // configure()

    private TreeMap<String, Color> createPalette() {
        TreeMap<String, Color> result = new TreeMap<>();

        result.put("Dark Olive",
                new Color(0xA2, 0xCD, 0x5A, TRANSPARENCY));
        result.put("Dark Sea Green",
                new Color(0x9B, 0xC0, 0x9B, TRANSPARENCY));
        result.put("Indian Red",
                new Color(0xCD, 0x55, 0x55, TRANSPARENCY));
        result.put("Light Sky Blue",
                new Color(0x8D, 0xB6, 0xCD, TRANSPARENCY));
        result.put("Pale Turquoise",
                new Color(0x66, 0x8B, 0x8B, TRANSPARENCY));
        result.put("Pale Violet Red",
                new Color(0xCD, 0x68, 0x69, TRANSPARENCY));
        result.put("Blanched Almond",
                new Color(0xFF, 0xEB, 0xCD, TRANSPARENCY));

        return result;
    } // createPalette()

    private TreeMap<String, Double> createSizes() {
        TreeMap<String, Double> sizes = new TreeMap<>();

        sizes.put("smallest", 0.1);
        sizes.put("smaller", 0.2);
        sizes.put("medium", 0.3);
        sizes.put("larger", 0.4);
        sizes.put("largest", 0.5);

        return sizes;
    } // createSizes()

    private TreeMap<String, Integer> createFigures() {
        TreeMap<String, Integer> result = new TreeMap<>();

        for (int numPoints = 5; numPoints <= 12; numPoints++) {
            String label = numPoints + "-pointed star";
            result.put(label, numPoints);
        } // for

        return result;
    } // createFigures()

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu colorMenu = new JMenu("Select color");
        menuBar.add(colorMenu);

        ActionListener colorListener = new ColorListener(this.artPanel,
                this.palette);
        for (String colorName : this.palette.keySet()) {
            addMenuItem(colorMenu, colorName, colorListener);
        } // for

        JMenu figureMenu = new JMenu("Select figure");
        menuBar.add(figureMenu);

        ActionListener figureListener = new FigureListener(this.artPanel,
                this.figures);
        for (String figureName : this.figures.keySet()) {
            addMenuItem(figureMenu, figureName, figureListener);
        } // for

        JMenu sizeMenu = new JMenu("Select size");
        menuBar.add(sizeMenu);

        ActionListener sizeListener = new SizeListener(this.artPanel,
                this.sizes);
        for (String sizeName : this.sizes.keySet()) {
            addMenuItem(sizeMenu, sizeName, sizeListener);
        } // for

        return menuBar;
    } // createMenuBar()

    private void addMenuItem(JMenu menu, String menuLabel,
            ActionListener listener) {
        JMenuItem menuItem = new JMenuItem(menuLabel);
        menu.add(menuItem);
        menuItem.addActionListener(listener);
        menuItem.setActionCommand(menuLabel);
    } // addMenuItem( JMenu, String )

    public static void main(String[] args) {
        Art art = new Art();
    } // main( String [] )

} // Art
