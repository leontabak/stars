package stars;

import java.awt.Shape;
import java.awt.geom.Area;

public class HollowStar {

    private static final double SMALL_BIG_RATIO = 0.6;

    private final Shape shape;

    public HollowStar(double centerX, double centerY, double radius,
            int numberOfPoints) {

        Star bigStar = new Star(centerX, centerY, radius, numberOfPoints);
        Star smallStar = new Star(centerX, centerY,
                SMALL_BIG_RATIO * radius, numberOfPoints);

        Area area = new Area(bigStar.getShape());
        area.subtract(new Area(smallStar.getShape()));

        this.shape = area;
    } // HollowStar( double, double, int )

    public Shape getShape() {
        return this.shape;
    } // getShape()
} // HollowStar
