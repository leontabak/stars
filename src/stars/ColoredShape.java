package stars;

import java.awt.Color;
import java.awt.Shape;

public class ColoredShape {

    private final Color color;
    private final Shape shape;

    public ColoredShape(Color color, Shape shape) {
        this.color = color;
        this.shape = shape;
    } // ColoredShape( Color, Shape )

    public Color getColor() {
        return this.color;
    } // getColor()

    public Shape getShape() {
        return this.shape;
    } // getShape()

} // ColorShape
