package stars;

import java.awt.Shape;
import java.awt.geom.Path2D;

public class Star {

    private static final double MINOR_MAJOR_RATIO = 0.6;

    private final Path2D path;

    public Star(double centerX, double centerY, double radius,
            int numberOfPoints) {

        this.path = new Path2D.Double();

        double majorRadius = radius;
        double minorRadius = MINOR_MAJOR_RATIO * radius;

        double angle = 0.0;
        double angularIncrement = Math.PI / numberOfPoints;

        double r = majorRadius;

        double x = centerX + r * Math.cos(angle);
        double y = centerY + r * Math.sin(angle);

        this.path.moveTo(x, y);

        for (int i = 1; i < 2 * numberOfPoints; i++) {
            angle += angularIncrement;

            if (i % 2 == 1) {
                r = minorRadius;
            } // if
            else {
                r = majorRadius;
            } // else

            x = centerX + r * Math.cos(angle);
            y = centerY + r * Math.sin(angle);

            this.path.lineTo(x, y);
        } // for

        this.path.closePath();
    } // Star( double, double, int )

    public Shape getShape() {
        return this.path;
    } // getShape()
} // Star
