package stars;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

public class SizeListener implements ActionListener {

    private final ArtPanel artPanel;
    private final TreeMap<String, Double> sizes;

    public SizeListener(ArtPanel artPanel,
            TreeMap<String, Double> sizes) {
        this.artPanel = artPanel;
        this.sizes = sizes;
    } // SizeListener()

    @Override
    public void actionPerformed(ActionEvent event) {
        String command = event.getActionCommand();

        double radius = this.sizes.get(command);

        this.artPanel.setRadius(radius);
    } // actionPerformed( ActionEvent )

} // SizeListener
