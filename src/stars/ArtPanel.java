package stars;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

public class ArtPanel extends JPanel implements MouseListener {

    private static final Color BG_COLOR = new Color(0x87CDFA);

    private Color figureColor;
    private int numberOfPoints;
    private final Point2D figureCenter;
    private double radius;
    private final List<ColoredShape> figures;

    public ArtPanel(Color color, int numberOfPoints, double radius) {
        this.figureColor = color;
        this.numberOfPoints = numberOfPoints;
        this.figureCenter = new Point2D.Double(0.0, 0.0);
        this.radius = radius;
        this.figures = new ArrayList<>();

        this.configure();
    } // ArtPanel()

    private void configure() {
        this.setBackground(BG_COLOR);
        this.addMouseListener(this);
    } // configure()

    public void setFigureColor(Color color) {
        this.figureColor = color;
    } // setFigureColor( Color )

    public void setNumberOfPoints(int figure) {
        this.numberOfPoints = figure;
    } // setNumberOfPoints( int )

    public void setRadius(double radius) {
        this.radius = radius;
    } // setRadius( double )

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;

        AffineTransform translate = new AffineTransform();
        translate.setToTranslation(1.0, 1.0);

        AffineTransform scale = new AffineTransform();
        int panelWidth = this.getWidth();
        int panelHeight = this.getHeight();
        scale.setToScale(panelWidth / 2, panelHeight / 2);

        AffineTransform transform = new AffineTransform();
        transform.concatenate(scale);
        transform.concatenate(translate);

        GeneralPath path = new GeneralPath();
        path.moveTo(-1.0, 1.0);
        path.lineTo(1.0, 1.0);
        path.lineTo(1.0, 0.4);
        path.lineTo(-1.0, 0.4);
        path.closePath();
        Shape ground = transform.createTransformedShape(path);
        Color grassColor = new Color(96, 224, 140);
        g2D.setColor(grassColor);
        g2D.fill(ground);

        for (ColoredShape coloredShape : this.figures) {

            Color color = coloredShape.getColor();
            Shape s = coloredShape.getShape();

            Shape shape = transform.createTransformedShape(s);

            g2D.setColor(color);
            g2D.fill(shape);

        } // for

    } // paintComponent( Graphics )

    @Override
    public void mouseClicked(MouseEvent e) {
        int xCoordinate = e.getX();
        int yCoordinate = e.getY();

        int panelWidth = this.getWidth();
        int panelHeight = this.getHeight();

        //  0.0 <= y <= 1.0
        double centerX = ((double) xCoordinate) / panelWidth;
        double centerY = ((double) yCoordinate) / panelHeight;

        // translate to a different coordinate system
        //  -1.0 <= centerX <= +1.0
        //  -1.0 <= centerY <= +1.0
        centerX = 2 * centerX - 1;
        centerY = 2 * centerY - 1;

        HollowStar star = new HollowStar(centerX, centerY, this.radius,
                this.numberOfPoints);
        Shape shape = star.getShape();

        this.figures.add(new ColoredShape(this.figureColor, shape));

        this.repaint();
    } // mouseClicked( MouseEvent )

    @Override
    public void mouseEntered(MouseEvent e) {
    } // mouseClicked( MouseEvent )

    @Override
    public void mouseExited(MouseEvent e) {
    } // mouseExited( MouseEvent )

    @Override
    public void mousePressed(MouseEvent e) {
    } // mousePressed( MouseEvent )

    @Override
    public void mouseReleased(MouseEvent e) {
    } // mouseReleased( MouseEvent )

} // ArtPanel
